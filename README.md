   # Task Details
Your task is to create a football information app supporting the following requirements: 
1.	A splash screen will be shown when the app is launched. Use whatever image you want for the splash screen. 
2.	The app’s main screen will show a list of football teams. When a team is selected, the app will show:
+	The team’s logo 
+	Names of players in the team 
+	A list of 10 (or fewer) upcoming matches for the team. You should show the name of the rival team, the date, and the competition (Champions League, Primera Division, etc.). 
+	Each of the above should be shown in its own View, but all should be visible in the main screen. Use scrollbars if needed.  
 
For this exercise, you should implement the app in React Native, so that it runs on both iOS and Android. Football information is available from https://www.football-data.org/. 


# Demo

![](footballInfoApp.gif)

# Install

```sh
$ npm install
$ react-native run-ios
$ react-native run-android
```


# [react-native-remote-svg](https://github.com/seekshiva/react-native-remote-svg)
react-native-remote-svg package gives an Image component that supports both svg and png file types.

# [moment](https://momentjs.com/)
A lightweight JavaScript date library for parsing, validating, manipulating, and formatting dates.

# [redux-saga](https://github.com/redux-saga/redux-saga)
redux-saga is a library that aims to make application side effects (i.e. asynchronous things like data fetching and impure things like accessing the browser cache) easier to manage, more efficient to execute, easy to test, and better at handling failures.

# [axios](https://github.com/axios/axios)
Promise based HTTP client for the browser and node.js
