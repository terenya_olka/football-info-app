export const MAIN_SCREEN = 'MAIN_SCREEN';
export const PARTICULAR_TEAM_SCREEN = 'PARTICULAR_TEAM_SCREEN';

export const SQUAD = 'Squad';
export const LOADING = 'LOADING';
export const MATCHES = 'MATCHES';