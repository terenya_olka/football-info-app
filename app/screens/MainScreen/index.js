import { connect } from 'react-redux';
import MainScreen from './MainScreen';
import {
  getFootballList,
  getParticularTeam,
  getMatchesForParticularTeam,
} from "../../redux/actions/index";

const mapStateToProps = (state) => {
  return {
    teams: state.footballTeamsReducer.teams,
    loading: state.footballTeamsReducer.loading,
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    getFootballList: () => dispatch(getFootballList()),
    getParticularTeam: (id) => dispatch(getParticularTeam(id)),
    getMatchesForParticularTeam: (id) => dispatch(getMatchesForParticularTeam(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);