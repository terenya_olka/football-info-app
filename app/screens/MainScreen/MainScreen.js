import React from 'react';
import { FlatList, Text, View, TouchableOpacity } from 'react-native';
import { PARTICULAR_TEAM_SCREEN } from '../../const/string';
import LoadingScreen from "../LoadingScreen/index";
import styles from './style';

const MainScreen = (props) => {
  React.useEffect(() => {
      props.getFootballList();
    }, []
  );

  const onPressTeam = async (id, name) => {
    try {
      await props.getParticularTeam(id);
      await props.getMatchesForParticularTeam(id);
      await props.navigation.navigate(PARTICULAR_TEAM_SCREEN, { particularTeam: name });
    } catch (error) {
      console.warn(error);
    }
  };

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => onPressTeam(item.id, item.name)}>
        <View style={styles.itemContainer}>
          <Text style={styles.itemText}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const keyExtractor = (item) => item.name + item.id;

  return (
    props.loading
      ? <LoadingScreen/>
      : <FlatList
        data={props.teams}
        renderItem={renderItem}
        keyExtractor={keyExtractor}
      />
  );
};

export default MainScreen;