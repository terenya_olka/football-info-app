import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  itemContainer: {
    borderBottomWidth: 1,
    borderColor: '#1955b7',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  itemText: {
    fontSize: 18,
  },
});