import React from 'react';
import { View } from 'react-native';
import { LOADING } from '../../const/string';
import Loading from '../../components/Loading/index';
import styles from './style';


const LoadingScreen = (props) => {
  return (
    <View style={styles.container}>
      <Loading
        title={LOADING}
        size="large"
      />
    </View>
  )
};

export default LoadingScreen;