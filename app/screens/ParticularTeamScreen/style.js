import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  itemContainer: {
    borderBottomWidth: 1,
    borderColor: '#1955b7',
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  itemSquadContainer: {
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
  itemText: {
    fontSize: 18,
  },
  logoContainer: {
    alignItems: 'center',
    paddingTop: 10,
  },
  logo: {
    height: 100,
    width: 100,
  },
  container: {
    flex: 1,
    paddingVertical: 10,
  },
  containerTitle: {
    textAlign: 'center',
    fontSize: 20,
    color: '#13278b',
  },
});