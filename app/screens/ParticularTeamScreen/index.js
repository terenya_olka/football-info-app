import { connect } from 'react-redux';
import ParticularTeamScreen from './ParticularTeamScreen';

const mapStateToProps = (state) => {
  return {
    team: state.particularTeamReducer.particularTeam,
    loading: state.particularTeamReducer.loading,
    matches: state.particularTeamReducer.matches,
  }
};

export default connect(mapStateToProps, null)(ParticularTeamScreen);