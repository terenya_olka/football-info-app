import React from 'react';
import { FlatList, Text, View, ScrollView } from 'react-native';
import moment from 'moment';
import Image from 'react-native-remote-svg';
import {
  SQUAD,
  MATCHES,
} from '../../const/string';
import LoadingScreen from "../LoadingScreen/index";
import styles from './style';

const ParticularTeamScreen = (props) => {
  const renderSquad = ({ item, index }) => {
    return (
      <View style={styles.itemSquadContainer}>
        <Text style={styles.itemText}>{`${index+1}. ${item.name}`}</Text>
      </View>
    );
  };

  const renderMatches = ({ item }) => {
    const date = moment(item.utcDate).format('DD.MM.YYYY');
    return (
      <View style={styles.itemContainer}>
        <Text style={styles.itemText}>{`${item.awayTeam.name} - ${item.homeTeam.name}`}</Text>
        <Text style={styles.itemText}>{`Competition - ${item.competition.name}`}</Text>
        <Text style={styles.itemText}>{`Date - ${date}`}</Text>
      </View>
    );
  };

  const keyExtractor = item => item.id;

  return (
    props.loading
      ? <LoadingScreen/>
      : <ScrollView>
        <View style={styles.logoContainer}>
          <Image
            style={styles.logo}
            source={props.team.crestUrl ? { uri: props.team.crestUrl } : require('../../../assets/images/ball.png')}
          />
        </View>
        {props.team.squad.length > 0
          ? <View style={styles.container}>
            <Text style={styles.containerTitle}>{SQUAD}</Text>
            <FlatList
              data={props.team.squad}
              renderItem={renderSquad}
              keyExtractor={keyExtractor}
            />
          </View>
          : null
        }
        {
          props.matches.length > 0 ?
            <View style={styles.container}>
              <Text style={styles.containerTitle}>{MATCHES}</Text>
              <FlatList
                data={props.matches}
                renderItem={renderMatches}
                keyExtractor={keyExtractor}
              />
            </View>
            : null
        }
    </ScrollView>
  );
};

export default ParticularTeamScreen;