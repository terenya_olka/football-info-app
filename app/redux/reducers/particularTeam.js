import {
  GET_PARTICULAR_TEAM,
  GET_PARTICULAR_TEAM_SUCCESS,
  GET_PARTICULAR_TEAM_ERROR,
  GET_MATCHES_FOR_PARTICULAR_TEAM,
  GET_MATCHES_FOR_PARTICULAR_TEAM_SUCCESS,
  GET_MATCHES_FOR_PARTICULAR_TEAM_ERROR,
} from "../../const/actions";

const initialState = {
  particularTeam: {},
  loading: false,
  error: '',
  matches: [],
};

const particularTeamReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PARTICULAR_TEAM: {
      return {
        ...state,
        loading: true,
        error: '',
      };
    }
    case GET_PARTICULAR_TEAM_SUCCESS: {
      return {
        ...state,
        particularTeam: action.data,
        loading: false,
        error: '',
      };
    }
    case GET_PARTICULAR_TEAM_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    }
    case GET_MATCHES_FOR_PARTICULAR_TEAM: {
      return {
        ...state,
        loading: true,
        error: '',
      };
    }
    case GET_MATCHES_FOR_PARTICULAR_TEAM_SUCCESS: {
      return {
        ...state,
        matches: action.data,
        loading: false,
        error: '',
      };
    }
    case GET_MATCHES_FOR_PARTICULAR_TEAM_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    }
  }
  return state;
};

export default particularTeamReducer;