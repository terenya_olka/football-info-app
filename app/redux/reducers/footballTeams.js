import {
  GET_FOOTBALL_TEAMS_LIST,
  GET_FOOTBALL_TEAMS_LIST_SUCCESS,
  GET_FOOTBALL_TEAMS_LIST_ERROR,
} from "../../const/actions";

const initialState = {
  teams: [],
  loading: false,
  error: '',
};

const footballTeamsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FOOTBALL_TEAMS_LIST: {
      return {
        ...state,
        loading: true,
        error: '',
      };
    }
    case GET_FOOTBALL_TEAMS_LIST_SUCCESS: {
      return {
        ...state,
        teams: action.data,
        loading: false,
        error: '',
      };
    }
    case GET_FOOTBALL_TEAMS_LIST_ERROR: {
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    }
  }
  return state;
};

export default footballTeamsReducer;