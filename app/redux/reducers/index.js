import { combineReducers } from 'redux';
import footballTeamsReducer from './footballTeams';
import particularTeamReducer from './particularTeam';

export default combineReducers({
  footballTeamsReducer,
  particularTeamReducer,
});