import axios from 'axios';
import { takeLatest, put, call } from 'redux-saga/effects';
import moment from 'moment';
import {
  getFootballListSuccess,
  getFootballListError,
  getParticularTeamSuccess,
  getParticularTeamError,
  getMatchesForParticularTeamSuccess,
  getMatchesForParticularTeamError,
} from '../actions/index';
import {
  GET_FOOTBALL_TEAMS_LIST,
  GET_PARTICULAR_TEAM,
  GET_MATCHES_FOR_PARTICULAR_TEAM,
} from '../../const/actions';

const fetchFootballTeamsApi = async () => {
  return await axios.get('https://api.football-data.org/v2/teams', {
    headers: {
      'content-type': 'application/json',
      'X-Auth-Token': '6c3646f3b5ca4033b439e955c06bdebb',
    }
  });
};

function* fetchFootballTeams() {
  try {
    const response = yield call(fetchFootballTeamsApi);
    yield put(getFootballListSuccess(response.data.teams));
  } catch (error) {
    yield put(getFootballListError(error));
  }
};

const fetchParticularTeamApi = async (team) => {
  const id = team.data;
  return await axios.get(`https://api.football-data.org/v2/teams/${id}`, {
    headers: {
      'content-type': 'application/json',
      'X-Auth-Token': '6c3646f3b5ca4033b439e955c06bdebb',
    }
  });
};

function* fetchParticularTeam(id) {
  try {
    const response = yield call(fetchParticularTeamApi, id);
    yield put(getParticularTeamSuccess(response.data));
  } catch (error) {
    yield put(getParticularTeamError(error));
  }
};

const fetchMatchesForParticularTeamApi = async (team) => {
  const id = team.data;
  return await axios.get(`https://api.football-data.org/v2/teams/${id}/matches?limit=10&dateFrom=${moment().format('YYYY-MM-DD')}&dateTo=${moment().add(1, 'Y').format('YYYY-MM-DD')}`, {
    headers: {
      'content-type': 'application/json',
      'X-Auth-Token': '6c3646f3b5ca4033b439e955c06bdebb',
    }
  });
};

function* fetchMatchesForParticularTeam(id) {
  try {
    const response = yield call(fetchMatchesForParticularTeamApi, id);
    yield put(getMatchesForParticularTeamSuccess(response.data.matches));
  } catch (error) {
    yield put(getMatchesForParticularTeamError(error));
  }
};

export function* Saga() {
  yield takeLatest(GET_FOOTBALL_TEAMS_LIST, fetchFootballTeams);
  yield takeLatest(GET_PARTICULAR_TEAM, fetchParticularTeam);
  yield takeLatest(GET_MATCHES_FOR_PARTICULAR_TEAM, fetchMatchesForParticularTeam);
};
