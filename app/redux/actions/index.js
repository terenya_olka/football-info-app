import {
  GET_FOOTBALL_TEAMS_LIST,
  GET_FOOTBALL_TEAMS_LIST_SUCCESS,
  GET_FOOTBALL_TEAMS_LIST_ERROR,
  GET_PARTICULAR_TEAM,
  GET_PARTICULAR_TEAM_SUCCESS,
  GET_PARTICULAR_TEAM_ERROR,
  GET_MATCHES_FOR_PARTICULAR_TEAM_ERROR,
  GET_MATCHES_FOR_PARTICULAR_TEAM,
  GET_MATCHES_FOR_PARTICULAR_TEAM_SUCCESS,
} from '../../const/actions';

export const getFootballList = () => {
  return {
    type: GET_FOOTBALL_TEAMS_LIST,
  }
};

export const getFootballListSuccess = (data) => {
  return {
    type: GET_FOOTBALL_TEAMS_LIST_SUCCESS,
    data: data,
  }
};

export const getFootballListError = (error) => {
  return {
    type: GET_FOOTBALL_TEAMS_LIST_ERROR,
    data: error,
  }
};

export const getParticularTeam = (id) => {
  return {
    type: GET_PARTICULAR_TEAM,
    data: id,
  }
};

export const getParticularTeamSuccess = (data) => {
  return {
    type: GET_PARTICULAR_TEAM_SUCCESS,
    data: data,
  }
};

export const getParticularTeamError = (error) => {
  return {
    type: GET_PARTICULAR_TEAM_ERROR,
    data: error,
  }
};

export const getMatchesForParticularTeam = (id) => {
  return {
    type: GET_MATCHES_FOR_PARTICULAR_TEAM,
    data: id,
  }
};

export const getMatchesForParticularTeamSuccess = (data) => {
  return {
    type: GET_MATCHES_FOR_PARTICULAR_TEAM_SUCCESS,
    data: data,
  }
};

export const getMatchesForParticularTeamError = (error) => {
  return {
    type: GET_MATCHES_FOR_PARTICULAR_TEAM_ERROR,
    data: error,
  }
};

