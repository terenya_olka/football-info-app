import React from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import styles from './style';

const Loading = (props) => {

  const {title, size} = props;

  return (
    <View style={styles.container}>
      <Text>{title}</Text>
      <ActivityIndicator size={size}/>
    </View>
  )
};

export default Loading;