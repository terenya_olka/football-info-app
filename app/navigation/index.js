import { createStackNavigator } from 'react-navigation-stack';
import {
  MAIN_SCREEN,
  PARTICULAR_TEAM_SCREEN,
} from '../const/string';
import MainScreen from '../screens/MainScreen';
import ParticularTeamScreen from '../screens/ParticularTeamScreen';

const MainNavigator = createStackNavigator(
  {
    [MAIN_SCREEN]: {
      screen: MainScreen,
      navigationOptions: ({ navigation }) => {
        return {
          title: 'Teams',
          headerTintColor: '#13278b',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }
      }
    },
    [PARTICULAR_TEAM_SCREEN]: {
      screen: ParticularTeamScreen,
      navigationOptions: ({ navigation }) => {
        return {
          title: navigation.getParam('particularTeam', 'Team'),
          headerTintColor: '#13278b',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }
      }
    },
  },
  {
    initialRouteName: MAIN_SCREEN,
  });

export default MainNavigator;