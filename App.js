/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import MainNavigator from './app/navigation/index';
import { createAppContainer } from 'react-navigation';
import store from './app/redux/storeConfig';
import { Saga } from './app/redux/saga/index';

console.disableYellowBox = true;

store.runSaga(Saga);
const AppNav = createAppContainer(MainNavigator);

const App = () => {

  React.useEffect(() => {
    SplashScreen && SplashScreen.hide();
  }, []);

  return (
    <Provider store={store}>
      <SafeAreaView style={{ flex: 1 }}>
        <AppNav />
      </SafeAreaView>
    </Provider>
  );
};

export default App;
